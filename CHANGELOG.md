# Changelog

## v0.3.0

- Rename from `git_semver` to `git_tags_semver` due to a name conflict on crates.io

## v0.2.0

- Use the new `resolver = "2"` feature resolver introduced with Rust 1.51 to join the `no_std` type definitions and the `std` helpers in a single package

## v0.1.0

- Initial version based on two crates in a single workspace: `no_std` type definitions and `std` helpers for `build.rs`
