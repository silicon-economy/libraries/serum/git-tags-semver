// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use regex::Regex;

/// Errors that can occur during execution of this crate.
#[derive(Debug)]
pub enum VersionError {
    /// The string to be parsed did not contain any git hash
    NoGitHash(String),
    /// The Semver information is incomplete
    IncompleteSemver,
    /// The Semver information contains invalid characters
    InvalidSemver,
    /// And error was found while building the regex
    Regex,
    /// The git-describe command returned an error
    Command,
    /// The string to be parsed was not valid UTF8
    UTF8,
}

impl std::error::Error for VersionError {}

impl std::fmt::Display for VersionError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
      VersionError::NoGitHash(git_string) => write!(f, "The output of the 'git describe' call should at least match a git hash. Your 'git describe' call produced: '{}'.", git_string
),
      VersionError::IncompleteSemver => write!(f, "Either major+minor+patch+commits or no semver fields at all should be available."),
      VersionError::InvalidSemver => write!(f, "The SemVer was containing character that are not u32!"),
      VersionError::Regex => write!(f, "Internal Regex error"),
      VersionError::Command => write!(f, "Could not run Git command"),
      VersionError::UTF8 => write!(f, "Could not parse git output as utf8"),
    }
    }
}

/// Generate a version file string containing the version information as `pub const VERSION`
pub fn generate_version_file() -> Result<String, VersionError> {
    let git_string = run_git_describe()?;
    let version = parse_git_describe(&git_string)?;
    let version_out = generate_version_string(version);

    Ok(format!(
        "pub const VERSION: git_tags_semver::GitVersion<'static> = {};",
        version_out
    ))
}

fn run_git_describe() -> Result<String, VersionError> {
    let git_string = std::process::Command::new("git")
        .args(&[
            "describe",
            "--always",
            "--dirty=-dirty",
            "--long",
            "--match=v[0-9]*.[0-9]*.[0-9]*",
            "--abbrev=8",
        ])
        .output()
        .map_err(|_| VersionError::Command)?
        .stdout;
    Ok(std::str::from_utf8(&git_string)
        .map_err(|_| VersionError::UTF8)?
        .trim()
        .to_string())
}

fn parse_git_describe(git_string: &str) -> Result<crate::GitVersion, VersionError> {
    // Use some regex magic to parse that info to a struct
    // Thanks to the --long option, there is only three main formats to match
    // (release, rc, no tag) each could be followed by a -dirty suffix.
    // Possible formats
    // v1.0.0-0-g01234567      exact release version
    // v1.0.0-rc-5-gabcdefed   behind rc version
    // 02468ace                no release made yet
    // <format>-dirty          dirty flag (applies to all of the above)

    let parser = Regex::new(
        r"(?x)                  # insignificant whitespace mode (for comments)
        ^                       # line start
        (v                      # the whole semver block starts with v 
          (?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)  # semver numbers
          (?P<rc>-rc)?          # '-rc' is optional
          -(?P<commits>\d+)     # '-<n>' mandatory commit-counter
          -g                    # '-g' precedes the hash if a tag was found
        )?                      # semver block is optional (missing if no
                                # tag / release was made yet)
        (?P<hash>[\w\d]{8})     # 8 alphanumeric digits for the hash
        (?P<dirty>-dirty)?      # optional '-dirty' flag
        $                       # line end (therefore, call trim() before)
        ",
    )
    .map_err(|_| VersionError::Regex)?;

    let captures = parser
        .captures(git_string)
        .ok_or_else(|| VersionError::NoGitHash(git_string.to_string()))?;

    // First, check if the semver information was found and generate code,
    // otherwise "None"
    let semver = match (
        captures.name("major"),
        captures.name("minor"),
        captures.name("patch"),
        captures.name("commits"),
    ) {
        (Some(major), Some(minor), Some(patch), Some(commits)) => {
            let rc = captures.name("rc").is_some();
            Some(crate::SemanticVersion {
                major: major
                    .as_str()
                    .parse::<u32>()
                    .map_err(|_| VersionError::InvalidSemver)?,
                minor: minor
                    .as_str()
                    .parse::<u32>()
                    .map_err(|_| VersionError::InvalidSemver)?,
                patch: patch
                    .as_str()
                    .parse::<u32>()
                    .map_err(|_| VersionError::InvalidSemver)?,
                rc,
                commits: commits
                    .as_str()
                    .parse::<u32>()
                    .map_err(|_| VersionError::InvalidSemver)?,
            })
        }
        (None, None, None, None) => None,
        _ => {
            return Err(VersionError::IncompleteSemver);
        }
    };

    // Then build the GitVersion which contains the semver info
    let hash_str = captures
        .name("hash")
        .expect("The git hash should be matched in any case.")
        .as_str();
    let hash = u32::from_str_radix(hash_str, 16)
        .expect("The git hash is not hexadecimal")
        .to_be_bytes();
    let dirty = captures.name("dirty").is_some();

    Ok(crate::GitVersion {
        semver,
        hash,
        dirty,
        git_string,
    })
}

fn generate_version_string(version: crate::GitVersion) -> String {
    let semver = if let Some(semver) = version.semver {
        format!("Some(git_tags_semver::SemanticVersion {{ major: {}, minor: {}, patch: {}, rc: {}, commits: {}}})", semver.major, semver.minor, semver.patch, semver.rc, semver.commits)
    } else {
        "None".to_string()
    };

    format!(
        "git_tags_semver::GitVersion {{ semver: {}, hash: {:#x?}, dirty: {}, git_string: \"{}\" }}",
        semver, version.hash, version.dirty, version.git_string
    )
}

#[cfg(test)]
mod tests {
    #[test]
    fn parser() -> Result<(), crate::VersionError> {
        //let git_string = "v0.1.0-0-g01234567";
        //let git_string = "v0.1.0-0-g01234567-dirty";
        //let git_string = "v0.1.0-rc-0-g01234567";
        //let git_string = "v0.1.0-rc-0-g01234567-dirty";
        //let git_string = "01234567";
        //let git_string = "01234567-dirty";

        assert_eq!(
            super::parse_git_describe("v0.1.0-0-g01234567")?,
            crate::GitVersion::<'static> {
                semver: Some(crate::SemanticVersion {
                    major: 0,
                    minor: 1,
                    patch: 0,
                    rc: false,
                    commits: 0
                }),
                hash: [0x01, 0x23, 0x45, 0x67],
                dirty: false,
                git_string: "v0.1.0-0-g01234567"
            }
        );
        assert_eq!(
            super::parse_git_describe("v0.1.0-0-g01234567-dirty")?,
            crate::GitVersion::<'static> {
                semver: Some(crate::SemanticVersion {
                    major: 0,
                    minor: 1,
                    patch: 0,
                    rc: false,
                    commits: 0
                }),
                hash: [0x01, 0x23, 0x45, 0x67],
                dirty: true,
                git_string: "v0.1.0-0-g01234567-dirty"
            }
        );
        assert_eq!(
            super::parse_git_describe("v0.1.0-rc-0-g01234567")?,
            crate::GitVersion::<'static> {
                semver: Some(crate::SemanticVersion {
                    major: 0,
                    minor: 1,
                    patch: 0,
                    rc: true,
                    commits: 0
                }),
                hash: [0x01, 0x23, 0x45, 0x67],
                dirty: false,
                git_string: "v0.1.0-rc-0-g01234567"
            }
        );
        assert_eq!(
            super::parse_git_describe("v0.1.0-rc-0-g01234567-dirty")?,
            crate::GitVersion::<'static> {
                semver: Some(crate::SemanticVersion {
                    major: 0,
                    minor: 1,
                    patch: 0,
                    rc: true,
                    commits: 0
                }),
                hash: [0x01, 0x23, 0x45, 0x67],
                dirty: true,
                git_string: "v0.1.0-rc-0-g01234567-dirty"
            }
        );

        assert_eq!(
            super::parse_git_describe("01234567")?,
            crate::GitVersion::<'static> {
                semver: None,
                hash: [0x01, 0x23, 0x45, 0x67],
                dirty: false,
                git_string: "01234567"
            }
        );

        assert_eq!(
            super::parse_git_describe("01234567-dirty")?,
            crate::GitVersion::<'static> {
                semver: None,
                hash: [0x01, 0x23, 0x45, 0x67],
                dirty: true,
                git_string: "01234567-dirty"
            }
        );

        Ok(())
    }
}
